'use strict';

/* App Module */

var mealizerApp = angular.module('mealizerApp', [
  'ngRoute',
  'phonecatAnimations',
  'mealizerControllers',
  'phonecatFilters',
  'phonecatServices'
]);

mealizerApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      //when('/', {
      //  templateUrl: 'partials/mealizer-form.html',
      //  controller: 'MealizerForm'
      //}).
      //when('/phones/:phoneId', {
      //  templateUrl: 'partials/phone-detail.html',
      //  controller: 'PhoneDetailCtrl'
      //}).
      when('/monday', {
         templateUrl: 'views/page1/index.html',
         activetab: 'page1'
         //controller: 'PhoneDetailCtrl'
      }).
      when('/tuesday', {
         templateUrl: 'views/page2/index.html',
         activetab: 'page2'
            //controller: 'PhoneDetailCtrl'
      })
      .otherwise({
        redirectTo: '/monday'
      });
  }]);
