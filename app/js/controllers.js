'use strict';

/* Controllers */

var mealizerControllers = angular.module('mealizerControllers', []);

mealizerApp.controller('MealizerForm', ['$scope', function($scope){
    $scope.user = {
        name: 'Vlad',
        surname: 'Shulhin'
    };
    $scope.greet = function () {
        alert(this.user.name);
    }
}]);

mealizerApp.controller('TabSwitching' , ['$scope', '$location', function($scope, $location, $route){

    $scope.redirect = function (page){
        $location.url('/'+page);
    };

    $scope.isActive = function(route) {
        return route === $location.path();
    }

}]);

//phonecatControllers.controller('PhoneListCtrl', ['$scope', 'Phone',
//  function($scope, Phone) {
//    $scope.phones = Phone.query();
//    $scope.orderProp = 'age';
//  }]);
//
//phonecatControllers.controller('PhoneDetailCtrl', ['$scope', '$routeParams', 'Phone',
//  function($scope, $routeParams, Phone) {
//    $scope.phone = Phone.get({phoneId: $routeParams.phoneId}, function(phone) {
//      $scope.mainImageUrl = phone.images[0];
//    });
//
//    $scope.setImage = function(imageUrl) {
//      $scope.mainImageUrl = imageUrl;
//    };
//  }]);
